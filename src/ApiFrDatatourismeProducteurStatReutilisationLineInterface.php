<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeProducteur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeProducteurStatReutilisationLineInterface interface file.
 * 
 * This represents a line of reutilisation data.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeProducteurStatReutilisationLineInterface extends Stringable
{
	
	/**
	 * Gets the name of the producer of the flux.
	 * 
	 * @return string
	 */
	public function getProducteur() : string;
	
	/**
	 * Gets the name of the diffuseur of the flux.
	 * 
	 * @return string
	 */
	public function getDiffuseur() : string;
	
	/**
	 * Gets the name of the type of diffuseur.
	 * 
	 * @return string
	 */
	public function getType() : string;
	
	/**
	 * Gets the code postal of the diffuseur.
	 * 
	 * @return ?string
	 */
	public function getCodePostal() : ?string;
	
	/**
	 * Gets the country of the diffuseur.
	 * 
	 * @return ?string
	 */
	public function getPays() : ?string;
	
	/**
	 * Gets the name of the mode of download.
	 * 
	 * @return string
	 */
	public function getModeTelechargement() : string;
	
	/**
	 * Gets the name of the diffuseur's application that uses the flux.
	 * 
	 * @return string
	 */
	public function getApplication() : string;
	
	/**
	 * Gets the url of the diffuseur's application.
	 * 
	 * @return ?UriInterface
	 */
	public function getUrl() : ?UriInterface;
	
	/**
	 * Gets when the flux was used.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateTelechargement() : DateTimeInterface;
	
	/**
	 * Gets the name of the primary type of point of interest in the flux.
	 * 
	 * @return string
	 */
	public function getTypePrincipal() : string;
	
	/**
	 * Gets the name of the secondary type of point of interest in the flux.
	 * 
	 * @return ?string
	 */
	public function getTypeSecondaire() : ?string;
	
	/**
	 * Gets the creator of the data from the producer.
	 * 
	 * @return string
	 */
	public function getCreateur() : string;
	
	/**
	 * Gets the total number of points of interest gathered in the flux that
	 * day.
	 * 
	 * @return int
	 */
	public function getTotal() : int;
	
}
