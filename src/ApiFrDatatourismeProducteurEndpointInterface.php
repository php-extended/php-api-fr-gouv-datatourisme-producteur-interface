<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeProducteur;

use InvalidArgumentException;
use Iterator;
use LogicException;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiFrDatatourismeProducteurEndpointInterface interface file.
 * 
 * This interface represents an endpoint for the producteur facade of the
 * datatourisme application.
 * 
 * @author Anastaszor
 */
interface ApiFrDatatourismeProducteurEndpointInterface extends Stringable
{
	
	/**
	 * Logs to the given hostname with the given username and password.
	 *
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 * @throws InvalidArgumentException if the credentials are wrong
	 * @throws RuntimeException if the login form cannot be submitted
	 * @throws ClientExceptionInterface if the connection cannot be established
	 */
	public function login(string $username, string $password) : bool;
	
	/**
	 * Copies the stats reutilisation file for today into the raw file
	 * data store.
	 *
	 * @param string $path the full path of the file to be created
	 * @return boolean
	 * @throws LogicException if called but not logged in
	 * @throws RuntimeException if the file cannot be written
	 * @throws ClientExceptionInterface if the file cannot be downloaded
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 */
	public function copyTodaysReutilisationStatsFile(string $path) : bool;
	
	/**
	 * Gets the reutilisation iterator from the given full file path.
	 *
	 * @param string $path the full path of the file to be read
	 * @return Iterator<integer, ApiFrDatatourismeProducteurStatReutilisationLineInterface>
	 * @throws InvalidArgumentException if the file does not exist
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 * @throws ReificationThrowable if the data objects cannot be built
	 */
	public function getReutilisationStatsFromFile(string $path) : Iterator;
	
	/**
	 * Gets directly the iterator from todays file, directly downloaded from
	 * the website.
	 * 
	 * @return Iterator<integer, ApiFrDatatourismeProducteurStatReutilisationLineInterface>
	 * @throws LogicException when called but not logged in
	 * @throws RuntimeException if the file cannot be written
	 * @throws ClientExceptionInterface if the file cannot be downloaded
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 * @throws ReificationThrowable if the data objects cannot be built
	 */
	public function getTodaysReutilisationStats() : Iterator;
	
}
